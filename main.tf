terraform {
  backend "remote" {
    organization = "varun2"

    workspaces {
      name = "my-aws-workspace"
    }
  }
}

provider "aws" {}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  # Idk what this means/does, I assume it's just the ami creator.
  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = "test-aws"

  tags = {
    Name = "devops-website"
  }
}
