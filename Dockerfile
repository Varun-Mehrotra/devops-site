FROM node
WORKDIR /code
EXPOSE 3000
COPY src/ .
RUN npm install
CMD ["npm", "start"]
